<?php

namespace App\Form;

use App\Entity\Candidate;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CandidateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('gender')
            ->add('firstName')
            ->add('lastName')
            ->add('email')
            ->add('updatedAt')
            ->add('createdAt')
            ->add('adress')
            ->add('country')
            ->add('nationality')
            ->add('cv')
            ->add('passport')
            ->add('profilPicture')
            ->add('currentLocation')
            ->add('dateOfBirth')
            ->add('placeOfBirth')
            ->add('password')
            ->add('availability')
            ->add('jobSector')
            ->add('experience')
            ->add('shortDescription')
            ->add('notes')
            ->add('deletedAt')
            ->add('candidate1')
            ->add('jobOffer')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Candidate::class,
        ]);
    }
}
