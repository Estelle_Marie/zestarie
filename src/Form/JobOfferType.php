<?php

namespace App\Form;

use App\Entity\JobOffer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class JobOfferType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('description')
            ->add('activated')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('reference')
            ->add('notes')
            ->add('jobTitle')
            ->add('jobType')
            ->add('location')
            ->add('jobCategory')
            ->add('closingDate')
            ->add('salary')
            ->add('delatedAt')
            ->add('client')
            ->add('candidate')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => JobOffer::class,
        ]);
    }
}
